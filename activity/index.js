let num = 2;
const getCube = Math.pow(num,3);
console.log(`The cube of ${num} is ${getCube}`);

//ARRAY DESTRUCTURING 

const fullAddress = [258, 'Washington Ave NW', 'California', 90011];

const [streetNum, streetName, city, zipCode] = fullAddress;

console.log(`I live at ${streetNum} ${streetName}, ${city} ${zipCode} `);


// OBJECT DESTRUCTURING

const animal = {
    name: 'Lolong',
    type: 'saltwater crocodile',
    weight: 1075,
    size: '20 ft 3 in'
}

const { name, type, weight, size } = animal;
console.log(`${name} was a ${type}. He weighed at  ${weight} kgs with a measurement of ${size}`);

var arrayNumbers = [1, 2, 3 ,4 ,5];

arrayNumbers.forEach((arrayNum) => {
    console.log(arrayNum);
})

function sumReducer(accumulator, currentvalue){
    return accumulator + currentvalue;
}

let sum = arrayNumbers.reduce(sumReducer);
console.log(sum);

class Dog {
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

const myDog = new Dog("Frankie", 5, 'Miniature Dachshund');
console.log(myDog);